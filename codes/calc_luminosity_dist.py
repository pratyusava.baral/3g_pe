import numpy as np
import optparse
import pickle
import os
import sys
from astropy.constants import *
import healpy as hp
import bilby
from scipy.optimize import fsolve
from injection import calculate_signal


# parse commands
parser = optparse.OptionParser()
parser.add_option("--mass1", dest="mass1", type="float")
parser.add_option("--mass2", dest="mass2", type="float")
parser.add_option("--theta_jn", dest="theta_jn", type="float")
parser.add_option("--phase", dest="phase", type="float")
parser.add_option("--ra", type="float")
parser.add_option("--dec", type="float")
parser.add_option("--geocent_time", dest="geocent_time", type="float")
parser.add_option("--nside", type='int')
parser.add_option("--psi", dest="psi", type="float")
parser.add_option("--snr", type="float")
(options, args) = parser.parse_args()

mass_detector_frame = True

flow = 5
fref = 100
srate = 4096
tc_offset = 1

def calc_snr(dl, m1, m2, theta_jn, phase, psi, ra, dec, geocent_time, snr):
    
    q = bilby.gw.conversion.component_masses_to_mass_ratio(m1, m2)
    m, l = np.loadtxt('../../../../codes/Mass_Vs_TidalDeformability_SLY.txt',dtype=float,unpack=True)
    z = bilby.gw.conversion.luminosity_distance_to_redshift(dl)

    mcz = bilby.gw.conversion.component_masses_to_chirp_mass(m1, m2) * (1+z)
    l1 = np.interp(m1,m,l)
    l2 = np.interp(m2,m,l)
    lt = bilby.gw.conversion.lambda_1_lambda_2_to_lambda_tilde(l1, l2, m1, m2)
    dlt = bilby.gw.conversion.lambda_1_lambda_2_to_delta_lambda_tilde(l1, l2, m1, m2)

    Interferometers = ['CE']
    injection_parameters = dict(
        chirp_mass=mcz, mass_ratio=q, chi_1=0., chi_2=0., lambda_tilde = lt, delta_lambda_tilde = dlt,
        ra=ra, dec=dec, luminosity_distance = dl,
        theta_jn=theta_jn, psi=psi, phase=phase, geocent_time=geocent_time
    )

    t0 = -(5.0 / 256.0) * mcz * GM_sun.value / c.value**3 * (np.pi * mcz * GM_sun.value / c.value**3 * flow)**(-8.0/3.0)

    seglen = 2**(np.int(np.log2(np.abs(t0)))+1)

    start_time = injection_parameters["geocent_time"] - seglen + tc_offset
    waveformname = "TaylorF2"

    # costruct ifos
    ifo = bilby.gw.detector.InterferometerList(Interferometers)[0]
    waveform_generator = bilby.gw.WaveformGenerator(
        duration=seglen, sampling_frequency=srate,
        frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
        waveform_arguments=dict(waveform_approximant=waveformname, reference_frequency=fref, minimum_frequency=flow)
    )

    # print out snr
    snr1 = 0.
    frequency_array, frequency_domain_strain = calculate_signal(
            ifo, waveform_generator, injection_parameters, start_time, flow,
            earth_rotation_time_delay=True,
            earth_rotation_beam_patterns=True,
            finite_size = True
        )
    ifo.set_strain_data_from_frequency_domain_strain(
            frequency_domain_strain, start_time=start_time, frequency_array=frequency_array
        )
    ifo.minimum_frequency = flow
    snr1 += ifo.optimal_snr_squared(signal=ifo.frequency_domain_strain)
    snr1 = np.sqrt(snr1.real)
    return snr1 - snr
    
dl = fsolve(calc_snr, 100*(calc_snr(100, options.mass1, options.mass2, options.theta_jn, options.phase, options.psi, options.ra, options.dec, options.geocent_time, options.snr)+options.snr)/options.snr, args = (options.mass1, options.mass2, options.theta_jn, options.phase, options.psi, options.ra, options.dec, options.geocent_time, options.snr),xtol = 1e-1)
print (dl[0])

