import numpy as np
import optparse
import pickle
import os
import sys
from astropy.constants import *
import healpy as hp
from injection import calculate_signal

# parse commands
parser = optparse.OptionParser()
parser.add_option("--mass1", dest="mass1", type="float", default = 1.44)
parser.add_option("--mass2", dest="mass2", type="float", default = 1.4)
parser.add_option("--theta_jn", dest="theta_jn", type="float")
parser.add_option("--phase", dest="phase", type="float",default = np.pi/2)
parser.add_option("--geocent_time", dest="geocent_time", type="float", default = 630696093)
parser.add_option("--nside", type='int', default = 2) # this is the healpy nside
parser.add_option("--psi", dest="psi", type="float",default = np.pi/2)
parser.add_option("--snr", type="float")
(options, args) = parser.parse_args()


nside = options.nside
# Prepare the healpy grid
npix = hp.pixelfunc.nside2npix(nside)
apix = hp.pixelfunc.nside2pixarea(nside)

theta, phi = hp.pix2ang(nside=nside, ipix=np.arange(npix))
ra = phi
dec = np.pi/2 - theta
mass1 = options.mass1
mass2 = options.mass2
theta_jn = options.theta_jn
phase = options.phase
geocent_time = options.geocent_time
psi = options.psi
snr = options.snr


try:
    os.system('mkdir -p ../run_directory/snr'+str(options.snr)+'/thetajn'+str(options.theta_jn)+'/CE/logs_dl')
except:
    pass

try:
    os.system('mkdir -p ../run_directory/snr'+str(options.snr)+'/thetajn'+str(options.theta_jn)+'/CE/results')
except:
    pass

f = open('../run_directory/snr'+str(options.snr)+'/thetajn'+str(options.theta_jn)+'/index_tmp.txt','w')
for i in range(npix):
    f.write("   --mass1 "+str(mass1)+"   --mass2 "+str(mass2)+"   --ra "+str(ra[i])+"   --dec "+str(dec[i])
            +"   --theta_jn "+str(theta_jn)+"   --psi "+str(psi)+ 
            "   --phase "+str(phase)+"   --geocent_time "+str(geocent_time)+"   --snr "+str(snr))
    f.write("\n")
f.close()    

os.system('cp generate_run_param.sub ../run_directory/snr'+str(options.snr)+'/thetajn'+str(options.theta_jn)+'/CE/.')
