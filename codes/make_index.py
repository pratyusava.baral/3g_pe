import numpy as np
import os
import optparse
import pickle
import bilby
import sys
sys.path.append('../../../codes')
from injection import calculate_signal
from likelihood import MBGravitationalWaveTransientThirdGeneration
from astropy.constants import *
import healpy as hp
import matplotlib.pyplot as plt

# parse commands
parser = optparse.OptionParser()
parser.add_option("--snr", type="float")
parser.add_option("--theta_jn", dest="theta_jn", type="float")

(options, args) = parser.parse_args()

f = open('../run_directory/snr'+str(options.snr)+'/thetajn'+str(options.theta_jn)+'/index_tmp.txt','r')
lines = f.readlines()
f.close()
DL = []
f = open('../run_directory/snr'+str(options.snr)+'/thetajn'+str(options.theta_jn)+'/index.txt','w')
for i,line in enumerate(lines):
    f.write(str(line.split('--snr')[0]))
    dl = np.loadtxt('../run_directory/snr'+str(options.snr)+'/thetajn'+str(options.theta_jn)+'/CE/logs_dl/'+str(i)+'.out')
    f.write('     --luminosity_distance '+str(dl))
    DL.append(dl)
    f.write("\n")
f.close()

os.system('mkdir ../run_directory/snr'+str(options.snr)+'/thetajn'+str(options.theta_jn)+'/CE/logs')
os.system('cp ce_pe.sub ../run_directory/snr'+str(options.snr)+'/thetajn'+str(options.theta_jn)+'/CE/.')
plt.savefig('../run_directory/snr'+str(options.snr)+'/thetajn'+str(options.theta_jn)+'/dl_hist.png')
