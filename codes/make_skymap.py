import numpy as np
import os
import optparse
from scipy.stats import beta
import h5py
import sys
 
os.system('mkdir tmp')

parser = optparse.OptionParser()
parser.add_option("--maxpts", dest="maxpts", type="int", default = 1000)
parser.add_option("--l_low", dest="l_low", type="int", default = 0) # start index of the lines in index file (inclusive)
parser.add_option("--l_high", dest="l_high", type="int", default = -1) # end index of the lines in index file (inclusive)
parser.add_option("--d_square_reweight", action = "store_true", default = False)
parser.add_option("--logd_reweight",action = "store_true", default = False)
parser.add_option("--path_to_results", type = "string", default="results/")
parser.add_option("--path_to_index", type = "string", default="../index.txt")
(options, args) = parser.parse_args()

f = open(options.path_to_index, 'r')
lines = f.readlines()
ra = []
dec = []
dl = []
area90 = []
area50 = []
vol50 = []
vol90 = []
snr = []

if options.l_high == -1:
    options.l_high = len(lines)

for i,line in enumerate(lines):
    print (i)
    if i<options.l_low or i>options.l_high:
        continue
    try:
        dl.append(float(line.split()[17]))
        ra.append(float(line.split()[5]))
        dec.append(float(line.split()[7]))
        ft = open('logs/'+str(i)+'.out')
        try:
            snr.append(float((ft.readlines()[0].split()[-1])[:-1]))
        except:
            snr.append(-999)
        ft.close()
        path = str(i)
        os.system('rm -rf tmp/*')
        data_source = options.path_to_results+path+'/None_result.json'
        os.system('cp ' + data_source+ ' tmp/.');
        make_h5_file = 'bilby_pipe_to_ligo_skymap_samples  tmp/None_result.json -o tmp/bilby_samples.hdf5'
        os.system(make_h5_file);
        if options.d_square_reweight:
                f = h5py.File('tmp/bilby_samples.hdf5','a')
                samples= np.array([f['posterior_samples']['ra'],f['posterior_samples']['dec'],f['posterior_samples']['luminosity_distance']]).T                
                samples_new = samples[np.random.choice(np.arange(len(samples)),size=options.maxpts,p=samples[:,2]**-1/np.sum(samples[:,2]**-1))]
                                
                hf = h5py.File('tmp/bilby_samples1.hdf5','w')
                dtype = (np.record, [('ra', '<f8'), ('dec', '<f8'), ('luminosity_distance', '<f8')])
                samples = []
                for i in range(len(samples_new)):
                        samples.append((samples_new[i,0],samples_new[i,1],samples_new[i,2]))                
                hf.create_dataset('posterior_samples',shape=(options.maxpts,),dtype=dtype,data=samples)
                hf.close()
                os.system('ligo-skymap-from-samples -o tmp/. --maxpts '+str(options.maxpts)+' tmp/bilby_samples1.hdf5');
                os.system('ligo-skymap-stats tmp/skymap.fits --contour 50 90 -o tmp/tmp.txt ');
                os.system('ligo-skymap-plot tmp/skymap.fits --contour 50 90 --radec '+str(np.degrees(ra[-1]))+' '+str(np.degrees(dec[-1]))+' --annotate -o tmp/skymap_d2a.png ');
                area50.append(np.loadtxt('tmp/tmp.txt', skiprows = 2, usecols=[6]))
                area90.append(np.loadtxt('tmp/tmp.txt', skiprows = 2, usecols=[7]))
                vol50.append(np.loadtxt('tmp/tmp.txt', skiprows = 2, usecols=[10]))
                vol90.append(np.loadtxt('tmp/tmp.txt', skiprows = 2, usecols=[11]))
                np.savetxt(options.path_to_results+path+'/summary_d2a',np.array([area50[-1],area90[-1],vol50[-1],vol90[-1],snr[-1]]),header = 'area_50 area_90 vol_50 vol_90 snr')
                os.system ('cp tmp/skymap_d2a.png '+options.path_to_results + path +'/.')
                os.system ('rm tmp/skymap_d2a.png')


        elif options.logd_reweight:
                f = h5py.File('tmp/bilby_samples.hdf5','a')
                samples= np.array([f['posterior_samples']['ra'],f['posterior_samples']['dec'],f['posterior_samples']['luminosity_distance']]).T                
                samples_new = samples[np.random.choice(np.arange(len(samples)),size=options.maxpts,p=samples[:,2]**2/np.sum(samples[:,2]**2))]
                
                hf = h5py.File('tmp/bilby_samples1.hdf5','w')
                dtype = (np.record, [('ra', '<f8'), ('dec', '<f8'), ('luminosity_distance', '<f8')])
                samples = []
                for i in range(len(samples_new)):
                        samples.append((samples_new[i,0],samples_new[i,1],samples_new[i,2]))
                hf.create_dataset('posterior_samples',shape=(options.maxpts,),dtype=dtype,data=samples)
                hf.close()
                os.system('ligo-skymap-from-samples -o tmp/. --maxpts '+str(options.maxpts)+' tmp/bilby_samples1.hdf5');
                os.system('ligo-skymap-stats tmp/skymap.fits --contour 50 90 -o tmp/tmp.txt ');
                os.system('ligo-skymap-plot tmp/skymap.fits --contour 50 90 --radec '+str(np.degrees(ra[-1]))+' '+str(np.degrees(dec[-1]))+' --annotate -o tmp/skymap_logd.png ');
                area50.append(np.loadtxt('tmp/tmp.txt', skiprows = 2, usecols=[6]))
                area90.append(np.loadtxt('tmp/tmp.txt', skiprows = 2, usecols=[7]))
                vol50.append(np.loadtxt('tmp/tmp.txt', skiprows = 2, usecols=[10]))
                vol90.append(np.loadtxt('tmp/tmp.txt', skiprows = 2, usecols=[11]))
                np.savetxt(options.path_to_results+path+'/summary_logd',np.array([area50[-1],area90[-1],vol50[-1],vol90[-1],snr[-1]]),header = 'area_50 area_90 vol_50 vol_90 snr')
                os.system ('cp tmp/skymap_logd.png '+ options.path_to_results + path +'/.')
                os.system ('rm tmp/skymap_logd.png')

        else:
                os.system('ligo-skymap-from-samples -o tmp/. --maxpts '+str(options.maxpts)+' tmp/bilby_samples.hdf5');
                os.system('ligo-skymap-stats tmp/skymap.fits --contour 50 90 -o tmp/tmp.txt ');
                os.system('ligo-skymap-plot tmp/skymap.fits --contour 50 90 --radec '+str(np.degrees(ra[-1]))+' '+str(np.degrees(dec[-1]))+' --annotate -o tmp/skymap.png ');
                area50.append(np.loadtxt('tmp/tmp.txt', skiprows = 2, usecols=[6]))
                area90.append(np.loadtxt('tmp/tmp.txt', skiprows = 2, usecols=[7]))
                vol50.append(np.loadtxt('tmp/tmp.txt', skiprows = 2, usecols=[10]))
                vol90.append(np.loadtxt('tmp/tmp.txt', skiprows = 2, usecols=[11]))
                np.savetxt(options.path_to_results+path+'/summary',np.array([area50[-1],area90[-1],vol50[-1],vol90[-1],snr[-1]]),header = 'area_50 area_90 vol_50 vol_90 snr')
                os.system ('cp tmp/skymap.png '+options.path_to_results + path +'/.')
                os.system ('rm tmp/skymap.png')
    except KeyboardInterrupt:
        sys.exit()
    except Exception as e:
        print ('!!WARNING!! ;Exception: ',e)
        pass
