__author__ = "Soichiro Morisaki; Pratyusava Baral"


import numpy as np
import lal
import lalsimulation

import bilby
from bilby.core.utils import ra_dec_to_theta_phi, speed_of_light

msun_to_seconds = lal.G_SI * lal.MSUN_SI / lal.C_SI**3.


def tau_up_to_2PN(f, m1, m2, chi1, chi2):
    """Taken from Eq. (3.3) of arXiv:gr-qc/9502040"""
    # mass combinations
    mtot = m1 + m2
    mtot_in_seconds = mtot * msun_to_seconds
    mc = (m1 * m2)**(3. / 5.) / (m1 + m2)**(1. / 5.)
    mc_in_seconds = mc * msun_to_seconds
    eta = m1 * m2 / (m1 + m2)**2.
    x = np.pi * mtot_in_seconds * f
    # spin combinations
    beta = ((113. * (m1 / mtot)**2. + 75. * eta) * chi1 + (113. * (m2 / mtot)**2. + 75. * eta) * chi2) / 12.
    sigma = (-247. * chi1 * chi2 + 721. * chi1 * chi2) * eta / 48.
    # up to 2PN
    tau0 = 5. / 256. * mc_in_seconds * (np.pi * mc_in_seconds * f)**(-8. / 3.)
    tau2 = 4. / 3. * (743. / 336. + 11. * eta / 4.) * x**(2. / 3.) * tau0
    tau3 = -8. / 5. * (4. * np.pi - beta) * x * tau0
    tau4 = 2. * (3058673. / 1016064. + 5429. * eta / 1008. + 617. * eta**2. / 144. - sigma) * x**(4. / 3.) * tau0
    return tau0 + tau2 + tau3 + tau4


def finite_size_factor(x, y):
    return 0.5 * (np.exp(-np.pi * 1j * x * (1. + y)) * np.sinc(x * (1 - y)) +
                  np.exp(np.pi * 1j * x * (1. - y)) * np.sinc(x * (1 + y)))


def calculate_signal(interferometer, waveform_generator, injection_parameters, start_time, minimum_frequency, earth_rotation_time_delay=True, earth_rotation_beam_patterns=True, finite_size=True):
    converted_injection_parameters, _ = \
        bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters(injection_parameters)
    waveform_polarizations = waveform_generator.frequency_domain_strain(converted_injection_parameters)
    frequencies = waveform_generator.frequency_array
    idxs_above_minimum_frequency = frequencies > (minimum_frequency - (frequencies[1] - frequencies[0]))
    tc = converted_injection_parameters["geocent_time"]

    # get siderial time at each frequency
    if earth_rotation_time_delay or earth_rotation_beam_patterns:
        m1, m2 = converted_injection_parameters["mass_1"], converted_injection_parameters["mass_2"]
        chi1 = converted_injection_parameters["a_1"] * np.cos(converted_injection_parameters["tilt_1"])
        chi2 = converted_injection_parameters["a_2"] * np.cos(converted_injection_parameters["tilt_2"])
        times_from_tc = -tau_up_to_2PN(frequencies[idxs_above_minimum_frequency], m1, m2, chi1, chi2)
        gmst_at_tc = lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(tc))
        day = 24. * 60. * 60.
        gmst_day_after = lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(tc + day))
        one_second_to_gmst = (gmst_day_after - gmst_at_tc) / day
        gmsts = gmst_at_tc + one_second_to_gmst * times_from_tc
        
    else:
        gmsts = np.ones(sum(idxs_above_minimum_frequency)) * lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(tc))

    # calculate basis vectors of GW frame
    ra = converted_injection_parameters['ra']
    dec = converted_injection_parameters['dec']
    psi = converted_injection_parameters['psi']
    thetas, phis = ra_dec_to_theta_phi(ra, dec, gmsts)
    cosphis = np.cos(phis)
    costhetas = np.cos(thetas)
    sinphis = np.sin(phis)
    sinthetas = np.sin(thetas)
    u = np.zeros(shape=(3, len(phis)))
    u[0] = cosphis * costhetas
    u[1] = costhetas * sinphis
    u[2] = -sinthetas
    v = np.zeros(shape=(3, len(phis)))
    v[0] = -sinphis
    v[1] = cosphis
    m = -u * np.sin(psi) - v * np.cos(psi)
    n = -u * np.cos(psi) + v * np.sin(psi)
    omegas = np.zeros(shape=(3, len(phis)))
    omegas[0] = sinthetas * cosphis
    omegas[1] = sinthetas * sinphis
    omegas[2] = costhetas

    # beam patterns
    pol_plus = np.einsum('ik,jk->ijk', m, m) - np.einsum('ik,jk->ijk', n, n)
    pol_cross = np.einsum('ik,jk->ijk', m, n) + np.einsum('ik,jk->ijk', n, m)
    
    if not finite_size:
        fps = np.einsum('ij,ijk->k', interferometer.geometry.detector_tensor, pol_plus)
        fcs = np.einsum('ij,ijk->k', interferometer.geometry.detector_tensor, pol_cross)
        if not earth_rotation_beam_patterns:
            fps = fps[-1] * np.ones(len(fps))
            fcs = fcs[-1] * np.ones(len(fcs))
    else:
        
        pol_plus = np.einsum('ik,jk->ijk', m, m) - np.einsum('ik,jk->ijk', n, n)
        pol_cross = np.einsum('ik,jk->ijk', m, n) + np.einsum('ik,jk->ijk', n, m)
        detector_tensor_xx = 0.5 * np.einsum('i,j->ij', interferometer.geometry.x, interferometer.geometry.x)
        detector_tensor_yy = 0.5 * np.einsum('i,j->ij', interferometer.geometry.y, interferometer.geometry.y)
        fpxx = np.einsum('ij,ijk->k', detector_tensor_xx, pol_plus)
        fpyy = np.einsum('ij,ijk->k', detector_tensor_yy, pol_plus)
        fcxx = np.einsum('ij,ijk->k', detector_tensor_xx, pol_cross)
        fcyy = np.einsum('ij,ijk->k', detector_tensor_yy, pol_cross)
        if not earth_rotation_beam_patterns:
            fpxx = fpxx[-1] * np.ones(len(fpxx))
            fpyy = fpyy[-1] * np.ones(len(fpyy))
            fcxx = fcxx[-1] * np.ones(len(fcxx))
            fcyy = fcyy[-1] * np.ones(len(fcyy))
            
        px = -np.dot(omegas.T, interferometer.geometry.x)
        py = -np.dot(omegas.T, interferometer.geometry.y)
        fL_over_c = frequencies[idxs_above_minimum_frequency] * interferometer.geometry.length * 10.**3. / speed_of_light
        Dxx = finite_size_factor(fL_over_c, px)
        Dyy = finite_size_factor(fL_over_c, py)
        fps = fpxx * Dxx - fpyy * Dyy
        fcs = fcxx * Dxx - fcyy * Dyy

    # time-shift factor
    dts = -np.dot(omegas.T, interferometer.geometry.vertex) / speed_of_light
    ifo_times = tc - start_time + dts
    if not earth_rotation_time_delay:
        ifo_times = ifo_times[-1]
    
    # multiply them
    h = np.zeros(len(frequencies), dtype=np.complex)
    h[idxs_above_minimum_frequency] = fps * waveform_polarizations['plus'][idxs_above_minimum_frequency] \
        + fcs * waveform_polarizations['cross'][idxs_above_minimum_frequency]
    h[idxs_above_minimum_frequency] *= np.exp(-1j * 2. * np.pi * frequencies[idxs_above_minimum_frequency] * ifo_times)

    return frequencies, h
