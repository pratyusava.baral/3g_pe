import numpy as np
import os
import optparse
from scipy.stats import beta
import h5py
import sys
import ligo.skymap.plot
from ligo.skymap.io import read_sky_map
from ligo.skymap import postprocess
from matplotlib.colors import rgb2hex as rgb2hex
import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.sans-serif'] = ['Bitstream Vera Sans']
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['figure.figsize'] = (16.0, 10.0)
matplotlib.rcParams['axes.unicode_minus'] = False

import seaborn as sns
sns.set_context('talk')
sns.set_style('ticks')
sns.set_palette('colorblind')
colors=sns.color_palette('colorblind')

def plot_skymap(file1, labels, label_rotations, events, colorn, ra, dec, outname='skymap.pdf'):

    fig = plt.figure(figsize=(6, 3), dpi=300)
    ax = fig.add_subplot(111, projection='astro hours mollweide')
    
    ax.grid()
    
    
    for i,event in enumerate(events):
        skymap1, _ = read_sky_map(file1[event])
        cfl1 = postprocess.find_greedy_credible_levels(skymap1)

        color=rgb2hex(colors[colorn[i]])
        ax.contour_hpx(cfl1, colors=color, linewidths=2, linestyles='solid', levels=[0.9,])
        #plt.text(labels[event][0][0], labels[event][0][1], event, fontsize=14, color=color, rotation=label_rotations[event][0])
       
    plt.plot(np.degrees(ra), np.degrees(dec), '*k', markersize=10, transform = ax.get_transform("world"))
    plt.savefig(outname)

s = 1000
f = open('thetajn0.2/index.txt', 'r')
lines = f.readlines()
ra = []
dec = []
os.system('mkdir tmp')

for i,line in enumerate(lines):
    print (i)
    os.system('rm -rf tmp/*')
    ra.append(float(line.split()[5]))
    dec.append(float(line.split()[7]))
    
    data_source = 'thetajn1.0/CE/results/'+str(i)+'/None_result.json'
    os.system('cp ' + data_source + ' tmp/.');
    
    make_h5_file = 'bilby_pipe_to_ligo_skymap_samples  tmp/None_result.json -o tmp/bilby_samples.hdf5'
    os.system(make_h5_file);
    f = h5py.File('tmp/bilby_samples.hdf5','a')
    samples= np.array([f['posterior_samples']['ra'],f['posterior_samples']['dec'],f['posterior_samples']['luminosity_distance']]).T                
    samples_new = samples[np.random.choice(np.arange(len(samples)),size=s,p=samples[:,2]**-1/np.sum(samples[:,2]**-1))]
                      
    hf = h5py.File('tmp/bilby_samples1.hdf5','w')
    dtype = (np.record, [('ra', '<f8'), ('dec', '<f8'), ('luminosity_distance', '<f8')])
    samples = []
    for j in range(len(samples_new)):
        samples.append((samples_new[j,0],samples_new[j,1],samples_new[j,2]))                
    hf.create_dataset('posterior_samples',shape=(s,),dtype=dtype,data=samples)
    hf.close()
    os.system('ligo-skymap-from-samples -o tmp/. --maxpts '+str(s)+' tmp/bilby_samples1.hdf5');
    os.system('mv tmp/skymap.fits tmp/skymap10.fits')
    dire10='tmp/skymap10.fits'
    f.close()
     
    
    os.system('rm tmp/*.json')
    os.system('rm tmp/*.hdf5')
    
    data_source = 'thetajn0.2/CE/results/'+str(i)+'/None_result.json'
    os.system('cp ' + data_source + ' tmp/.');
    make_h5_file = 'bilby_pipe_to_ligo_skymap_samples  tmp/None_result.json -o tmp/bilby_samples.hdf5'
    os.system(make_h5_file);
    f = h5py.File('tmp/bilby_samples.hdf5','a')
    samples= np.array([f['posterior_samples']['ra'],f['posterior_samples']['dec'],f['posterior_samples']['luminosity_distance']]).T                
    samples_new = samples[np.random.choice(np.arange(len(samples)),size=s,p=samples[:,2]**-1/np.sum(samples[:,2]**-1))]
                      
    hf = h5py.File('tmp/bilby_samples1.hdf5','w')
    dtype = (np.record, [('ra', '<f8'), ('dec', '<f8'), ('luminosity_distance', '<f8')])
    samples = []
    for j in range(len(samples_new)):
        samples.append((samples_new[j,0],samples_new[j,1],samples_new[j,2]))                
    hf.create_dataset('posterior_samples',shape=(s,),dtype=dtype,data=samples)
    hf.close()
    os.system('ligo-skymap-from-samples -o tmp/. --maxpts '+str(s)+' tmp/bilby_samples1.hdf5');
    
    dire02='tmp/skymap.fits'
    file1 = {
    r'---- $\theta_{\rm JN} = 1.0$': dire10,
    r'---- $\theta_{\rm JN} = 0.2$': dire02}
    

    labels = {
    r'---- $\theta_{\rm JN} = 1.0$': [(5,950)],
    r'---- $\theta_{\rm JN} = 0.2$': [(5, 880)]}

    label_rotations = {
    r'---- $\theta_{\rm JN} = 1.0$': [0] ,   
    r'---- $\theta_{\rm JN} = 0.2$': [0]}
    

    events = [r'---- $\theta_{\rm JN} = 1.0$', r'---- $\theta_{\rm JN} = 0.2$']
    plot_skymap(file1, labels, label_rotations, events, colorn=[0,1], ra = ra[-1], dec = dec[-1], outname='plots/'+str(i)+'.pdf')
    #sys.exit()
   