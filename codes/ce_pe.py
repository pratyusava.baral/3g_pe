'''
    This code implements parameter estimation for CBCs in the 3G Era.
    Copyright (C) 2023 Soichiro Morisaki, Pratyusava Baral

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

'''

import numpy as np
import os
import optparse
import pickle
import bilby
import sys
from injection import calculate_signal
from likelihood import MBGravitationalWaveTransientThirdGeneration
from astropy.constants import *

# parse commands
parser = optparse.OptionParser()
parser.add_option("--mass1", dest="mass1", type="float")
parser.add_option("--mass2", dest="mass2", type="float")
parser.add_option("--mass-detector-frame", action="store_true", default=False, dest="mass_detector_frame")
parser.add_option("--luminosity_distance", dest="luminosity_distance", type="float")
parser.add_option("--ra", dest="ra", type="float")
parser.add_option("--dec", dest="dec", type="float")
parser.add_option("--theta_jn", dest="theta_jn", type="float")
parser.add_option("--phase", dest="phase", type="float")
parser.add_option("--geocent_time", dest="geocent_time", type="float")
parser.add_option("--psi", dest="psi", type="float")
parser.add_option("--nlive", dest="nlive", type="int", default = 4000)
parser.add_option("--dlogz", dest="dlogz", type="float")
parser.add_option("--randomseed", dest="randomseed", type="int", default=569)
parser.add_option("--outdir", dest="outdir", type="string")
parser.add_option("--flow", dest="flow", type="float", default=5. )
parser.add_option("--srate", dest="srate", type="float", default=4096.)
parser.add_option("--fref", dest="fref", type="float", default=100.)
parser.add_option("--tc_offset", dest="tc_offset", type="float", default=1.)
parser.add_option('-1',"--no_earth_rotation_time_delay_inj", dest="ertd_inj", action="store_false", default=True)
parser.add_option('-4',"--no_earth_rotation_time_delay_ana", dest="ertd_ana", action="store_false", default=True)
parser.add_option('-2',"--no_earth_rotation_beam_patterns_inj", dest="erbp_inj", action="store_false", default=True)
parser.add_option('-5',"--no_earth_rotation_beam_patterns_ana", dest="erbp_ana", action="store_false", default=True)
parser.add_option('-3',"--no_finite_size_inj", dest="fs_inj", action="store_false", default=True)
parser.add_option('-6',"--no_finite_size_ana", dest="fs_ana", action="store_false", default=True)
parser.add_option("--no_phase_marginalization", dest="pm", action="store_false", default=True)
parser.add_option("--distance_marginalization", dest="dm", action="store_true", default=False)
parser.add_option("--label", dest="label", type="string")
parser.add_option("--sample-CE-arrival-time", action="store_true", default=False, dest="sample_CE_arrival_time")
#parser.add_option("--only_intrinsic", action="store_true", default=False, dest="only_intrinsic") !! FIX ME!!!
parser.add_option("--only_extrinsic", action="store_true", default=False, dest="only_extrinsic")
parser.add_option("--only_extrinsic_plus_chirp_mass", action="store_true", default=False, dest="only_extrinsic_plus_chirp_mass")


(options, args) = parser.parse_args()

np.random.seed(options.randomseed)

outdir = options.outdir 

if not os.path.exists(outdir):
    os.mkdir(outdir)

# parameters
flow = options.flow
srate = options.srate
fref = options.fref
tc_offset = options.tc_offset

z = bilby.gw.conversion.luminosity_distance_to_redshift(options.luminosity_distance)

m1 = options.mass1
m2 = options.mass2

q = bilby.gw.conversion.component_masses_to_mass_ratio(m1, m2)
m, l = np.loadtxt('../../../../codes/Mass_Vs_TidalDeformability_SLY.txt',dtype=float,unpack=True)  



if options.mass_detector_frame:
    mcz = bilby.gw.conversion.component_masses_to_chirp_mass(m1, m2) 
    m1/=(1+z)
    m2/=(1+z)
    l1 = np.interp(m1,m,l)
    l2 = np.interp(m2,m,l)
    lt = bilby.gw.conversion.lambda_1_lambda_2_to_lambda_tilde(l1, l2, m1, m2)
    dlt = bilby.gw.conversion.lambda_1_lambda_2_to_delta_lambda_tilde(l1, l2, m1, m2)
else:
    mcz = bilby.gw.conversion.component_masses_to_chirp_mass(m1, m2) * (1+z)
    l1 = np.interp(m1,m,l)
    l2 = np.interp(m2,m,l)
    lt = bilby.gw.conversion.lambda_1_lambda_2_to_lambda_tilde(l1, l2, m1, m2)
    dlt = bilby.gw.conversion.lambda_1_lambda_2_to_delta_lambda_tilde(l1, l2, m1, m2)

ra = options.ra
dec = options.dec
theta_jn = options.theta_jn
psi = options.psi
phase=options.phase
geocent_time=options.geocent_time
Interferometers = ['CE']
luminosity_distance = options.luminosity_distance 
injection_parameters = dict(
    chirp_mass=mcz, mass_ratio=q, chi_1=0., chi_2=0., lambda_tilde = lt, delta_lambda_tilde = dlt,
    ra=ra, dec=dec, luminosity_distance = luminosity_distance,
    theta_jn=theta_jn, psi=psi, phase=phase, geocent_time=geocent_time
)

t0 = -(5.0 / 256.0) * mcz * GM_sun.value / c.value**3 * (np.pi * mcz * GM_sun.value / c.value**3 * flow)**(-8.0/3.0)

seglen = 2**(np.int(np.log2(np.abs(t0)))+1)

start_time = injection_parameters["geocent_time"] - seglen + tc_offset
waveformname = "TaylorF2"

# costruct ifos
ifos = bilby.gw.detector.InterferometerList(Interferometers)
waveform_generator = bilby.gw.WaveformGenerator(
    duration=seglen, sampling_frequency=srate,
    frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
    parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
    waveform_arguments=dict(waveform_approximant=waveformname, reference_frequency=fref, minimum_frequency=flow)
)

# print out snr
snr = 0.
for ifo in ifos:
    frequency_array, frequency_domain_strain = calculate_signal(
        ifo, waveform_generator, injection_parameters, start_time, flow,
        earth_rotation_time_delay=options.ertd_inj,
        earth_rotation_beam_patterns=options.erbp_inj,
        finite_size = options.fs_inj
    )
    ifo.set_strain_data_from_frequency_domain_strain(
        frequency_domain_strain, start_time=start_time, frequency_array=frequency_array
    )
    ifo.minimum_frequency = flow
    snr += ifo.optimal_snr_squared(signal=ifo.frequency_domain_strain)
snr = np.sqrt(snr.real)
print(f"The injected SNR is {snr}.")

# construct priors
priors = bilby.gw.prior.BNSPriorDict()
for key in ["mass_1", "mass_2", "lambda_1", "lambda_2"]:
    priors.pop(key)
    
mcmin, mcmax = injection_parameters["chirp_mass"] - 1e-5, injection_parameters["chirp_mass"] + 1e-5
priors["chirp_mass"] = bilby.core.prior.Uniform(name='chirp_mass', minimum=mcmin, maximum=mcmax)
priors["mass_ratio"] = bilby.core.prior.Uniform(name='mass_ratio', minimum=0.125, maximum=1)
priors["chi_1"] = 0
priors["chi_2"] = 0
priors["luminosity_distance"] = bilby.core.prior.Uniform(name='luminosity_distance', minimum=max(1,injection_parameters['luminosity_distance']-2000), maximum=max(injection_parameters['luminosity_distance']+2000, 3*injection_parameters['luminosity_distance']), unit='Mpc', latex_label='$d_L$')
priors['lambda_tilde'] = bilby.core.prior.Uniform(name = 'lambda_tilde', minimum = max(0,injection_parameters['lambda_tilde']-100), maximum = injection_parameters['lambda_tilde']+100)
priors['delta_lambda_tilde'] = bilby.core.prior.Uniform(injection_parameters['delta_lambda_tilde']-5000, injection_parameters['delta_lambda_tilde']+5000, name = 'delta_lambda_tilde')

if options.only_extrinsic or options.only_extrinsic_plus_chirp_mass:
    priors["mass_ratio"] = injection_parameters["mass_ratio"]
    priors['lambda_tilde'] = injection_parameters['lambda_tilde']
    priors['delta_lambda_tilde'] = injection_parameters['delta_lambda_tilde']
    if options.only_extrinsic:
        priors['chirp_mass'] = injection_parameters["chirp_mass"]
    


if options.sample_CE_arrival_time:
    reference_ifo = bilby.gw.detector.get_empty_interferometer("CE")
    injection_parameters["CE_time"] = injection_parameters['geocent_time'] + reference_ifo.time_delay_from_geocenter(
        ra=injection_parameters['ra'], dec=injection_parameters['dec'], time=injection_parameters['geocent_time']
    )
    priors['CE_time'] = bilby.core.prior.Uniform(
        minimum=injection_parameters["CE_time"] - 0.01,
        maximum=injection_parameters["CE_time"] + 0.01,
        name='CE_time', latex_label='$t_{CE}$', unit='$s$'
    )
else:
    priors['geocent_time'] = bilby.core.prior.Uniform(
        minimum=injection_parameters['geocent_time'] - 0.1,
        maximum=injection_parameters['geocent_time'] + 0.1,
        name='geocent_time', latex_label='$t_c$', unit='$s$'
    )
    

# set up likelihood
distance_marginalization=options.dm
phase_marginalization=options.pm
path_to_likelihood = os.path.join(outdir, f"likelihood_{options.label}.pickle")
if not os.path.exists(path_to_likelihood):
    search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
        duration=seglen, sampling_frequency=srate,
        frequency_domain_source_model=bilby.gw.source.binary_neutron_star_frequency_sequence,
        waveform_arguments=dict(waveform_approximant=waveformname, reference_frequency=fref),
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters
    )
    if options.sample_CE_arrival_time:
        time_reference = "CE"
    else:
        time_reference = "geocent"

    likelihood = MBGravitationalWaveTransientThirdGeneration(
        interferometers=ifos, waveform_generator=search_waveform_generator, priors=priors,
        reference_chirp_mass=mcmin, linear_interpolation=True,
        distance_marginalization=distance_marginalization,
        phase_marginalization=phase_marginalization, 
        time_reference=time_reference,
        earth_rotation_time_delay=options.ertd_ana,
        earth_rotation_beam_patterns=options.erbp_ana,
        finite_size = options.fs_ana
    )
    pickle.dump(likelihood, open(path_to_likelihood, "wb"))
else:
    likelihood = pickle.load(open(path_to_likelihood, "rb"))
    if distance_marginalization:
        priors['luminosity_distance'] = float(priors['luminosity_distance'].rescale(0.5))
    if phase_marginalization:
        priors["phase"] = 0.0

# sampling
result = bilby.run_sampler(
    likelihood=likelihood, priors=priors, sampler='dynesty', use_ratio=True,
    nlive=options.nlive, walks=100, maxmcmc=5000, nact=5, npool=16,
    injection_parameters=injection_parameters,
    outdir=outdir, label=options.label, dlogz=options.dlogz
)

# Make a corner plot.
result.plot_corner()
