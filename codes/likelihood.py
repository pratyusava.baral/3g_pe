import numpy as np
import lal
import lalsimulation

import bilby
from bilby.core.utils import ra_dec_to_theta_phi, speed_of_light
from bilby.gw.likelihood import MBGravitationalWaveTransient

msun_to_seconds = lal.G_SI * lal.MSUN_SI / lal.C_SI**3.


def tau_up_to_2PN(f, m1, m2, chi1, chi2):
    """Taken from Eq. (3.3) of arXiv:gr-qc/9502040"""
    # mass combinations
    mtot = m1 + m2
    mtot_in_seconds = mtot * msun_to_seconds
    mc = (m1 * m2)**(3. / 5.) / (m1 + m2)**(1. / 5.)
    mc_in_seconds = mc * msun_to_seconds
    eta = m1 * m2 / (m1 + m2)**2.
    x = np.pi * mtot_in_seconds * f
    # spin combinations
    beta = ((113. * (m1 / mtot)**2. + 75. * eta) * chi1 + (113. * (m2 / mtot)**2. + 75. * eta) * chi2) / 12.
    sigma = (-247. * chi1 * chi2 + 721. * chi1 * chi2) * eta / 48.
    # up to 2PN
    tau0 = 5. / 256. * mc_in_seconds * (np.pi * mc_in_seconds * f)**(-8. / 3.)
    tau2 = 4. / 3. * (743. / 336. + 11. * eta / 4.) * x**(2. / 3.) * tau0
    tau3 = -8. / 5. * (4. * np.pi - beta) * x * tau0
    tau4 = 2. * (3058673. / 1016064. + 5429. * eta / 1008. + 617. * eta**2. / 144. - sigma) * x**(4. / 3.) * tau0
    return tau0 + tau2 + tau3 + tau4




class MBGravitationalWaveTransientThirdGeneration(MBGravitationalWaveTransient):

    def __init__(
        self, interferometers, waveform_generator, reference_chirp_mass, highest_mode=2, linear_interpolation=True,
        accuracy_factor=5, time_offset=None, delta_f_end=None, maximum_banding_frequency=None,
        minimum_banding_duration=0., distance_marginalization=False, phase_marginalization=False, priors=None,
        distance_marginalization_lookup_table=None, reference_frame="sky", time_reference="geocenter",
        response_update=4., earth_rotation_beam_patterns=True, earth_rotation_time_delay=True, finite_size=True
    ):
        super(MBGravitationalWaveTransientThirdGeneration, self).__init__(
            interferometers=interferometers, waveform_generator=waveform_generator,
            reference_chirp_mass=reference_chirp_mass, highest_mode=highest_mode,
            linear_interpolation=linear_interpolation, accuracy_factor=accuracy_factor,
            time_offset=time_offset, delta_f_end=delta_f_end,
            maximum_banding_frequency=maximum_banding_frequency,
            minimum_banding_duration=minimum_banding_duration,
            distance_marginalization=distance_marginalization, phase_marginalization=phase_marginalization,
            priors=priors, distance_marginalization_lookup_table=distance_marginalization_lookup_table,
            reference_frame=reference_frame, time_reference=time_reference
        )
        self.response_update = response_update
        self._setup_detector_response_update_times()
        self._setup_exp_factors()
        self.earth_rotation_beam_patterns = earth_rotation_beam_patterns
        self.earth_rotation_time_delay = earth_rotation_time_delay
        self.finite_size = finite_size

    def _tau_to_f(self, tau):
        return (1. / (np.pi * self.reference_chirp_mass_in_second)) * \
            (256. * tau / (5. * self.reference_chirp_mass_in_second))**(-3. / 8.)

    def _setup_detector_response_update_times(self):
        number_of_updates = int(self._tau(self.minimum_frequency) // self.response_update) + 1
        times_to_update_from_tc = (np.arange(number_of_updates) - number_of_updates + 1) * self.response_update
        self.frequencies_at_updates = np.append(self._tau_to_f(-times_to_update_from_tc[:-1]), np.inf)
        self.idxs_detector_response = np.zeros(len(self.banded_frequency_points), dtype=int)
        for f in self.frequencies_at_updates[:-1]:
            self.idxs_detector_response += self.banded_frequency_points > f
#        reference_gpstime = (self.priors['geocent_time'].minimum + self.priors['geocent_time'].maximum) / 2.
        reference_gpstime = (self.priors[f'{self.time_reference}_time'].minimum + self.priors[f'{self.time_reference}_time'].maximum) / 2.
        day = 24. * 60. * 60.
        self.one_second_to_gmst = (lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(day + reference_gpstime)) - lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(reference_gpstime))) / day

    def _setup_exp_factors(self):
        self.fL_over_c = dict(
            (i.name, self.banded_frequency_points * i.geometry.length * 10.**3. / speed_of_light)
            for i in self.interferometers
        )
        self._exp_factor = dict((i.name, np.exp(np.pi * 1j * self.fL_over_c[i.name])) for i in self.interferometers)
        self._exp_factor_conj = dict((i.name, np.conj(self._exp_factor[i.name])) for i in self.interferometers)

    def _finite_size_factor(self, interferometer, pn):
        x = self.fL_over_c[interferometer.name]
        y = pn
        exp_pix = self._exp_factor[interferometer.name]
        exp_pix_conj = self._exp_factor_conj[interferometer.name]
        exp_pixy_conj = np.exp(-np.pi * 1j * x * y)
        sinc_pix_1_minus_y = np.sinc(x * (1 - y))
        sinc_pix_1_plus_y = np.sinc(x * (1 + y))
        return 0.5 * (exp_pix_conj * exp_pixy_conj * sinc_pix_1_minus_y + exp_pix * exp_pixy_conj * sinc_pix_1_plus_y)

    def calculate_snrs(self, waveform_polarizations, interferometer):
        """
        Compute the snrs for multi-banding

        Parameters
        ----------
        waveform_polarizations: waveform
        interferometer: bilby.gw.detector.Interferometer

        Returns
        -------
        snrs: named tuple of snrs

        """
        # calculate siderial times
        gmst_at_tc = lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(self.parameters['geocent_time']))
        converted_parameters, _ = \
            bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters(self.parameters)
        m1, m2 = converted_parameters["mass_1"], converted_parameters["mass_2"]
        chi1 = converted_parameters["a_1"] * np.cos(converted_parameters["tilt_1"])
        chi2 = converted_parameters["a_2"] * np.cos(converted_parameters["tilt_2"])
        times_to_update_from_tc = np.append(-tau_up_to_2PN(self.frequencies_at_updates[:-1], m1, m2, chi1, chi2), 0.)
        gmsts_to_update = self.one_second_to_gmst * times_to_update_from_tc + lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(self.parameters['geocent_time']))
        gmsts_to_update = np.fmod(gmsts_to_update, 2. * np.pi)
        ra, dec, psi = self.parameters['ra'], self.parameters['dec'], self.parameters['psi']
        thetas, phis = ra_dec_to_theta_phi(ra, dec, gmsts_to_update)
        cosphis = np.cos(phis)
        costhetas = np.cos(thetas)
        sinphis = np.sin(phis)
        sinthetas = np.sin(thetas)
        # basis vectors of GW frame
        u = np.zeros(shape=(3, len(phis)))
        u[0] = cosphis * costhetas
        u[1] = costhetas * sinphis
        u[2] = -sinthetas
        v = np.zeros(shape=(3, len(phis)))
        v[0] = -sinphis
        v[1] = cosphis
        m = -u * np.sin(psi) - v * np.cos(psi)
        n = -u * np.cos(psi) + v * np.sin(psi)
        omegas = np.zeros(shape=(3, len(phis)))
        omegas[0] = sinthetas * cosphis
        omegas[1] = sinthetas * sinphis
        omegas[2] = costhetas
        
        # calculate beam patterns
        pol_plus = np.einsum('ik,jk->ijk', m, m) - np.einsum('ik,jk->ijk', n, n)
        pol_cross = np.einsum('ik,jk->ijk', m, n) + np.einsum('ik,jk->ijk', n, m)
        
                
        if not self.finite_size:
            fps = np.einsum('ij,ijk->k', interferometer.geometry.detector_tensor, pol_plus)[self.idxs_detector_response]
            fcs = np.einsum('ij,ijk->k', interferometer.geometry.detector_tensor, pol_cross)[self.idxs_detector_response]
            if not self.earth_rotation_beam_patterns:
                fps = fps[-1] * np.ones(len(fps))
                fcs = fcs[-1] * np.ones(len(fcs))
                
        else:
            pol_plus = np.einsum('ik,jk->ijk', m, m) - np.einsum('ik,jk->ijk', n, n)
            pol_cross = np.einsum('ik,jk->ijk', m, n) + np.einsum('ik,jk->ijk', n, m)
            detector_tensor_xx = 0.5 * np.einsum('i,j->ij', interferometer.geometry.x, interferometer.geometry.x)
            detector_tensor_yy = 0.5 * np.einsum('i,j->ij', interferometer.geometry.y, interferometer.geometry.y)
            fpxx = np.einsum('ij,ijk->k', detector_tensor_xx, pol_plus)[self.idxs_detector_response]
            fpyy = np.einsum('ij,ijk->k', detector_tensor_yy, pol_plus)[self.idxs_detector_response]
            fcxx = np.einsum('ij,ijk->k', detector_tensor_xx, pol_cross)[self.idxs_detector_response]
            fcyy = np.einsum('ij,ijk->k', detector_tensor_yy, pol_cross)[self.idxs_detector_response]
            if not self.earth_rotation_beam_patterns:
                fpxx = fpxx[-1] * np.ones(len(fpxx))
                fpyy = fpyy[-1] * np.ones(len(fpyy))
                fcxx = fcxx[-1] * np.ones(len(fcxx))
                fcyy = fcyy[-1] * np.ones(len(fcyy))
            
            px = -np.dot(omegas.T, interferometer.geometry.x)[self.idxs_detector_response]
            py = -np.dot(omegas.T, interferometer.geometry.y)[self.idxs_detector_response]
            Dxx = self._finite_size_factor(interferometer, px)
            Dyy = self._finite_size_factor(interferometer, py)
            fps = fpxx * Dxx - fpyy * Dyy
            fcs = fcxx * Dxx - fcyy * Dyy
            
        # calculate time shifts
        dts = - np.dot(omegas.T, interferometer.geometry.vertex) / speed_of_light
        ifo_times = self.parameters['geocent_time'] - interferometer.strain_data.start_time + dts
        if not self.earth_rotation_time_delay:
            
            ifo_times = ifo_times[-1] * np.ones(len(ifo_times))

        h = fps * waveform_polarizations['plus'][self.unique_to_original_frequencies] \
            + fcs * waveform_polarizations['cross'][self.unique_to_original_frequencies]
        h *= np.exp(-1j * 2. * np.pi * self.banded_frequency_points * ifo_times[self.idxs_detector_response])

        d_inner_h = np.dot(h, self.linear_coeffs[interferometer.name])

        if self.linear_interpolation:
            optimal_snr_squared = np.vdot(np.real(h * np.conjugate(h)), self.quadratic_coeffs[interferometer.name])
        else:
            optimal_snr_squared = 0.
            for b in range(len(self.fb_dfb) - 1):
                Ks, Ke = self.Ks_Ke[b]
                start_idx, end_idx = self.start_end_idxs[b]
                Mb = self.Mbs[b]
                if b == 0:
                    optimal_snr_squared += (4. / self.interferometers.duration) * np.vdot(
                        np.real(h[start_idx:end_idx + 1] * np.conjugate(h[start_idx:end_idx + 1])),
                        interferometer.frequency_mask[Ks:Ke + 1] * self.windows[start_idx:end_idx + 1]
                        / interferometer.power_spectral_density_array[Ks:Ke + 1])
                else:
                    self.wths[interferometer.name][b][Ks:Ke + 1] = self.square_root_windows[start_idx:end_idx + 1] \
                        * h[start_idx:end_idx + 1]
                    self.hbcs[interferometer.name][b][-Mb:] = np.fft.irfft(self.wths[interferometer.name][b])
                    thbc = np.fft.rfft(self.hbcs[interferometer.name][b])
                    optimal_snr_squared += (4. / self.Tbhats[b]) * np.vdot(
                        np.real(thbc * np.conjugate(thbc)), self.Ibcs[interferometer.name][b])

        complex_matched_filter_snr = d_inner_h / (optimal_snr_squared**0.5)

        return self._CalculatedSNRs(
            d_inner_h=d_inner_h, optimal_snr_squared=optimal_snr_squared,
            complex_matched_filter_snr=complex_matched_filter_snr,
            d_inner_h_squared_tc_array=None,
            d_inner_h_array=None,
            optimal_snr_squared_array=None)
